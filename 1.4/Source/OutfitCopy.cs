﻿using System.Linq;
using RimWorld;
using Verse;
using UnityEngine;
using HarmonyLib;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System;

namespace AssignmentCopy
{
    [StaticConstructorOnStartup]
    internal static class HarmonyInit
    {
        static HarmonyInit()
        {
            new Harmony("Haecriver.OutfitCopy").PatchAll();
        }
    }

    public static class Patches
    {
        internal static string GetUniqueLabel<T>(string label, List<T> database, Func<T, string> getLabel)
        {
            string newLabel = label;
            Regex reNum = new Regex(@"^(.*?)\d+$");
            if (reNum.IsMatch(label))
            {
                newLabel = reNum.Replace(label, @"$1");
            }

            int i = 1;
            if (database != null)
            {
                do
                {
                    newLabel = newLabel + i++;
                }
                while (database.Any(l => getLabel(l) == newLabel));
            }
            else
            {
                newLabel = newLabel + i++;
            }

            return newLabel;
        }

        [HarmonyPatch(typeof(Dialog_ManageOutfits), "DoWindowContents")]
        public class Dialog_ManageOutfitsCopyAdder
        {
            public static void Prefix(ref Outfit ___selOutfitInt, Dialog_ManageOutfits __instance, Rect inRect)
            {
                float num = 470f;
                num += 10f;
                Rect rect = new Rect(num, 0f, 150f, 35f);
                num += 150f;
                Log.Warning("hello");
                if (Widgets.ButtonText(rect, "AC_CopyOutfit".Translate())) {
                    if (___selOutfitInt != null)
                    {
                        // Create a new name
                        string newLabel = GetUniqueLabel(___selOutfitInt.label, Current.Game.outfitDatabase.AllOutfits, (outfit) => outfit.label);

                        // Create outfit
                        Outfit newOutfit = Current.Game.outfitDatabase.MakeNewOutfit();
                        newOutfit.label = newLabel;
                        newOutfit.filter.CopyAllowancesFrom(___selOutfitInt.filter);

                        // Set copy as actual
                        ___selOutfitInt = newOutfit;
                        __instance.DoWindowContents(inRect);
                    }
                }
            }
        }

        [HarmonyPatch(typeof(Dialog_ManageDrugPolicies), "DoWindowContents")]
        public class Dialog_ManageDrugPoliciesCopyAdder
        {
            public static void Prefix(ref DrugPolicy ___selPolicy, Dialog_ManageDrugPolicies __instance, Rect inRect)
            {
                float num = 470f;
                num += 10f;
                Rect rect = new Rect(num, 0f, 150f, 35f);
                num += 150f;
                if (Widgets.ButtonText(rect, "AC_CopyDrugPolicy".Translate()))
                {
                    if (___selPolicy != null)
                    {
                        // Create a new name
                        string newLabel = GetUniqueLabel(___selPolicy.label, Current.Game.drugPolicyDatabase.AllPolicies, (drugPolicy) => drugPolicy.label);

                        // Create drugPolicy
                        DrugPolicy newDrugPolicy = Current.Game.drugPolicyDatabase.MakeNewDrugPolicy();
                        newDrugPolicy.label = newLabel;

                        // For each drug newDrugPolicy
                        for (int i = 0; i < ___selPolicy.Count; ++i)
                        {
                            // I cannot access to MemberWise Copy ... So I'll do it myself
                            newDrugPolicy[i].drug = ___selPolicy[i].drug;
                            newDrugPolicy[i].allowedForAddiction = ___selPolicy[i].allowedForAddiction;
                            newDrugPolicy[i].allowedForJoy = ___selPolicy[i].allowedForJoy;
                            newDrugPolicy[i].allowScheduled = ___selPolicy[i].allowScheduled;
                            newDrugPolicy[i].daysFrequency = ___selPolicy[i].daysFrequency;
                            newDrugPolicy[i].onlyIfMoodBelow = ___selPolicy[i].onlyIfMoodBelow;
                            newDrugPolicy[i].onlyIfJoyBelow = ___selPolicy[i].onlyIfJoyBelow;
                            newDrugPolicy[i].takeToInventory = ___selPolicy[i].takeToInventory;
                            newDrugPolicy[i].takeToInventoryTempBuffer = ___selPolicy[i].takeToInventoryTempBuffer;
                        }
                       
                        // Set copy as actual
                        ___selPolicy = newDrugPolicy;
                        __instance.DoWindowContents(inRect);
                    }
                }
            }
        }

        [HarmonyPatch(typeof(Dialog_ManageFoodRestrictions), "DoWindowContents")]
        public class Dialog_ManageFoodRestrictionsCopyAdder
        {
            public static void Prefix(ref FoodRestriction ___selFoodRestrictionInt, Dialog_ManageFoodRestrictions __instance, Rect inRect)
            {
                float num = 470f;
                num += 10f;
                Rect rect = new Rect(num, 0f, 150f, 35f);
                num += 150f;
                if (Widgets.ButtonText(rect, "AC_CopyFoodRestriction".Translate()))
                {
                    if (___selFoodRestrictionInt != null)
                    {
                        // Create a new name
                        string newLabel = GetUniqueLabel(
                            ___selFoodRestrictionInt.label, 
                            Current.Game.foodRestrictionDatabase.AllFoodRestrictions,
                            (foodRestriction) => foodRestriction.label
                        );

                        // Create outfit
                        FoodRestriction newOutfit = Current.Game.foodRestrictionDatabase.MakeNewFoodRestriction();
                        newOutfit.label = newLabel;
                        newOutfit.filter.CopyAllowancesFrom(___selFoodRestrictionInt.filter);

                        // Set copy as actual
                        ___selFoodRestrictionInt = newOutfit;
                        __instance.DoWindowContents(inRect);
                    }
                }
            }
        }

    }
}
